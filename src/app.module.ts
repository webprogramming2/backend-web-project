import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { ProductsModule } from './products/products.module';
import { PromotionsModule } from './promotions/promotions.module';
import { MembersModule } from './members/members.module';
import { BranchsModule } from './branchs/branchs.module';
import { ReceiptsModule } from './receipts/receipts.module';
import { StocksModule } from './stocks/stocks.module';
import { BillcostsModule } from './billcosts/billcosts.module';
import { SalarysModule } from './salarys/salarys.module';
import { CheckinoutsModule } from './checkinouts/checkinouts.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Billcost } from './billcosts/entities/billcost.entity';
import { Checkinout } from './checkinouts/entities/checkinout.entity';
import { User } from './users/entities/user.entity';
import { Branch } from './branchs/entities/branch.entity';
import { TypebillcostsModule } from './typebillcosts/typebillcosts.module';
import { Typebillcost } from './typebillcosts/entities/typebillcost.entity';
import { Stock } from './stocks/entities/stock.entity';
import { Member } from './members/entities/member.entity';
import { DataSource } from 'typeorm';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { Product } from './products/entities/product.entity';
import { Promotion } from './promotions/entities/promotion.entity';
import { Receipt } from './receipts/entities/receipt.entity';
import { ReceiptItem } from './receipts/entities/receiptItem.entity';
import { WorktimesModule } from './worktimes/worktimes.module';
import { AuthModule } from './auth/auth.module';
import { CheckstocksModule } from './checkstocks/checkstocks.module';
import { Checkstock } from './checkstocks/entities/checkstock.entity';
import { CheckStockItem } from './checkstocks/entities/checkstockItems.entity';
import { ReceiptstocksModule } from './receiptstocks/receiptstocks.module';
import { ReceiptStock } from './receiptstocks/entities/receiptstock.entity';
import { ReceiptStockItem } from './receiptstocks/entities/receiptstockItem.entity';
import { Worktime } from './worktimes/entities/worktime.entity';

@Module({
  imports: [
    // TypeOrmModule.forRoot({
    //   type: 'mysql',
    //   host: 'localhost',
    //   port: 3306,
    //   username: 'webpro',
    //   password: 'Pass@1234',
    //   database: 'webpro',
    //   entities: [
    //     Billcost,
    //     Checkinout,
    //     User,
    //     Branch,
    //     Typebillcost,
    //     Stock,
    //     Member,
    //     Product,
    //     Promotion,
    //     Receipt,
    //     ReceiptItem,
    //     Checkstock,
    //     CheckStockItem,
    //     ReceiptStock,
    //     ReceiptStockItem,
    //   ], //อย่าลืมใส่ entities นะครับทุกคน
    //   synchronize: true,
    // }),
    //--------------sqlite-------------
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'database.sqlite',
      synchronize: true,
      logging: false,
      entities: [
        Billcost,
        Checkinout,
        User,
        Branch,
        Typebillcost,
        Stock,
        Member,
        Product,
        Promotion,
        Receipt,
        ReceiptItem,
        Checkstock,
        CheckStockItem,
        ReceiptStock,
        ReceiptStockItem,
      ],
    }),
    // ---------------------------------
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    UsersModule,
    ProductsModule,
    PromotionsModule,
    MembersModule,
    BranchsModule,
    ReceiptsModule,
    StocksModule,
    BillcostsModule,
    SalarysModule,
    CheckinoutsModule,
    TypebillcostsModule,
    WorktimesModule,
    AuthModule,
    CheckstocksModule,
    ReceiptstocksModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
