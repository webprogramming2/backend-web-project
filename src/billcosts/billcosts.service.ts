import { Injectable } from '@nestjs/common';
import { CreateBillcostDto } from './dto/create-billcost.dto';
import { UpdateBillcostDto } from './dto/update-billcost.dto';
import { Billcost } from './entities/billcost.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class BillcostsService {
  constructor(
    @InjectRepository(Billcost)
    private billcostsRepository: Repository<Billcost>,
  ) {}
  create(createBillcostDto: CreateBillcostDto) {
    const billcost = new Billcost();
    billcost.Typebillcost = JSON.parse(createBillcostDto.Typebillcost);
    billcost.Billtotal = parseFloat(createBillcostDto.Billtotal);
    billcost.User = JSON.parse(createBillcostDto.User);
    billcost.Date = new Date(createBillcostDto.Date);
    billcost.Time = new Date(
      `${createBillcostDto.Date} ${createBillcostDto.Time}`,
    );

    billcost.branch = JSON.parse(createBillcostDto.branch);
    if (createBillcostDto.image && createBillcostDto.image !== '') {
      billcost.image = createBillcostDto.image;
    }
    return this.billcostsRepository.save(billcost);
  }

  findAll() {
    return this.billcostsRepository.find({
      relations: ['User', 'branch', 'Typebillcost'],
    });
  }

  findAllByTypebillcost(TypebillcostId: number) {
    return this.billcostsRepository.find({
      where: { Typebillcost: { id: TypebillcostId } },
      relations: ['User', 'branch', 'Typebillcost'],
      // order: { created: 'ASC' },
    });
  }

  findAllByUser(userId: number) {
    return this.billcostsRepository.find({
      where: { User: { id: userId } },
      relations: ['User', 'branch', 'Typebillcost'],
      // order: { created: 'ASC' },
    });
  }

  findAllByBranch(branchId: number) {
    return this.billcostsRepository.find({
      where: { branch: { id: branchId } },
      relations: ['User', 'branch', 'Typebillcost'],
      // order: { created: 'ASC' },
    });
  }

  findAllByFullName(fullName: string) {
    return this.billcostsRepository.find({
      where: { User: { fullName } },
      relations: ['User', 'branch', 'Typebillcost'],
    });
  }

  findOne(id: number) {
    return this.billcostsRepository.findOne({
      where: { id },
      relations: ['User', 'branch', 'Typebillcost'],
    });
  }

  async update(id: number, updateBillcostDto: UpdateBillcostDto) {
    const billcost = await this.billcostsRepository.findOneOrFail({
      where: { id },
    });
    billcost.Typebillcost = JSON.parse(updateBillcostDto.Typebillcost);
    billcost.Billtotal = parseFloat(updateBillcostDto.Billtotal);
    billcost.User = JSON.parse(updateBillcostDto.User);
    billcost.Date = new Date(updateBillcostDto.Date);
    billcost.Time = new Date(
      `${updateBillcostDto.Date} ${updateBillcostDto.Time}`,
    );
    billcost.branch = JSON.parse(updateBillcostDto.branch);
    if (updateBillcostDto.image && updateBillcostDto.image !== '') {
      billcost.image = updateBillcostDto.image;
    }
    this.billcostsRepository.save(billcost);
    return billcost;
  }

  async remove(id: number) {
    const deleteProduct = await this.billcostsRepository.findOneOrFail({
      where: { id },
    });
    await this.billcostsRepository.remove(deleteProduct);

    return deleteProduct;
  }
}
