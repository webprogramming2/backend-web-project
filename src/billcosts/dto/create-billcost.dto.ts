export class CreateBillcostDto {
  Typebillcost: string;
  Billtotal: string;
  User: string;
  Date: Date;
  Time: Date;
  branch: string;
  image: string;
}
