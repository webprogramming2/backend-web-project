import { Branch } from 'src/branchs/entities/branch.entity';
import { Typebillcost } from 'src/typebillcosts/entities/typebillcost.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';

@Entity()
export class Billcost {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Typebillcost, (typebillcost) => typebillcost.Typebill)
  @JoinColumn({ name: 'TypebillcostId' })
  Typebillcost: Typebillcost[];

  @Column()
  Billtotal: number;

  @ManyToOne(() => User, (user) => user.billcosts)
  User: User;

  @Column({ type: 'date' }) //YYYY-MM-DD
  Date: Date;

  @Column({ type: 'time' }) //HH:MM:SS
  Time: Date;

  @ManyToOne(() => Branch, (branch) => branch.billcosts)
  @JoinColumn({ name: 'branchId' })
  branch: Branch;

  @Column({ default: 'noimage.jpg' })
  image: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;
}
