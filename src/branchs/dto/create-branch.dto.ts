import { User } from 'src/users/entities/user.entity';

export class CreateBranchDto {
  name: string;
  Address: string;
  manager: User;
  tel: string;
}
