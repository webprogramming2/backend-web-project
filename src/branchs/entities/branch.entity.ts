import { Billcost } from 'src/billcosts/entities/billcost.entity';
import { Checkinout } from 'src/checkinouts/entities/checkinout.entity';
import { Receipt } from 'src/receipts/entities/receipt.entity';
import { Stock } from 'src/stocks/entities/stock.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';
@Entity()
export class Branch {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  Address: string;

  @Column()
  tel: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(() => User, (user) => user.branch)
  manager: User[];

  @OneToMany(() => Billcost, (billcost) => billcost.branch)
  billcosts: Billcost[];

  @OneToMany(() => Stock, (stock) => stock.branch)
  stocks: Stock[];

  @OneToMany(() => Receipt, (receipt) => receipt.branch)
  receipts: Receipt[];

  @OneToMany(() => Checkinout, (checkinout) => checkinout.branch)
  checkinouts: Checkinout[];
}
