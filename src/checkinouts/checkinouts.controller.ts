import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { CheckinoutsService } from './checkinouts.service';
import { CreateCheckinoutDto } from './dto/create-checkinout.dto';
import { UpdateCheckinoutDto } from './dto/update-checkinout.dto';
import { AuthGuard } from 'src/auth/auth.guard';

@UseGuards(AuthGuard)
@Controller('checkinouts')
export class CheckinoutsController {
  constructor(private readonly checkinoutsService: CheckinoutsService) {}

  @Post()
  create(@Body() createCheckinoutDto: CreateCheckinoutDto) {
    return this.checkinoutsService.create(createCheckinoutDto);
  }

  @Get()
  findAll() {
    return this.checkinoutsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.checkinoutsService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateCheckinoutDto: UpdateCheckinoutDto,
  ) {
    return this.checkinoutsService.update(+id, updateCheckinoutDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.checkinoutsService.remove(+id);
  }
}
