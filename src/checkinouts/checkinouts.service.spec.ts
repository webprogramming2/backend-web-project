import { Test, TestingModule } from '@nestjs/testing';
import { CheckinoutsService } from './checkinouts.service';

describe('CheckinoutsService', () => {
  let service: CheckinoutsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CheckinoutsService],
    }).compile();

    service = module.get<CheckinoutsService>(CheckinoutsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
