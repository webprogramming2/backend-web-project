export class CreateCheckinoutDto {
  date: Date;
  check_in: string;
  check_out: string;
  hour: number;
}
