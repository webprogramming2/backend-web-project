import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
@Entity()
export class Checkinout {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, (user) => user.checkinouts)
  user: User;

  @Column({ type: 'time' })
  timeIn: Date;

  @Column({ type: 'time' })
  timeOut: Date;

  @Column({ type: 'date' }) //YYYY-MM-DD
  date: Date;

  @ManyToOne(() => Branch, (branch) => branch.checkinouts)
  branch: Branch;

  @Column()
  date: Date;

  @Column({ type: 'time' })
  check_in: string;

  @Column({ type: 'time' })
  check_out: string;

  @Column()
  hour: number;
}
