import { Injectable } from '@nestjs/common';
import { CreateCheckstockDto } from './dto/create-checkstock.dto';
import { UpdateCheckstockDto } from './dto/update-checkstock.dto';
import { Checkstock } from './entities/checkstock.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Stock } from 'src/stocks/entities/stock.entity';
import { User } from 'src/users/entities/user.entity';
import { CheckStockItem } from './entities/checkstockItems.entity';

@Injectable()
export class CheckstocksService {
  @InjectRepository(Checkstock)
  private checkStockRepository: Repository<Checkstock>;
  @InjectRepository(Stock) private stocksRepository: Repository<Stock>;
  @InjectRepository(User) private usersRepository: Repository<User>;
  @InjectRepository(CheckStockItem)
  private checkStockItemRepository: Repository<CheckStockItem>;
  async create(createCheckstockDto: CreateCheckstockDto) {
    console.log('in create');
    const checkstock = new Checkstock();

    const user = await this.usersRepository.findOneBy({
      id: createCheckstockDto.userId,
    });
    checkstock.user = user;
    checkstock.date = new Date(createCheckstockDto.date);
    checkstock.checkStockItems = [];
    for (const i of createCheckstockDto.checkStockItems) {
      const checkStockItem = new CheckStockItem();
      const stock = new Stock();
      checkStockItem.stock = await this.stocksRepository.findOneBy({
        id: i.stockId,
      });
      checkStockItem.name = checkStockItem.stock.name;
      checkStockItem.minimum = checkStockItem.stock.minimum;
      checkStockItem.price = checkStockItem.stock.price;
      checkStockItem.unit = checkStockItem.stock.unit;
      checkStockItem.branch = checkStockItem.stock.branch;
      checkStockItem.balance = checkStockItem.stock.balance;
      // eslint-disable-next-line prettier/prettier
      checkStockItem.amount = i.amount;

      await this.checkStockItemRepository.save(checkStockItem);
      checkstock.checkStockItems.push(checkStockItem);
      console.log('in lopp check stock');
      stock.name = checkStockItem.name;
      stock.minimum = checkStockItem.minimum;
      stock.price = checkStockItem.price;
      stock.unit = checkStockItem.unit;
      stock.branch = checkStockItem.branch;
      stock.balance = checkStockItem.amount;
      await this.stocksRepository.update(i.stockId, stock);
    }
    // this.stocksRepository.save(stock);
    return this.checkStockRepository.save(checkstock);
  }

  findAll() {
    return this.checkStockRepository.find({
      relations: { user: true, checkStockItems: true },
    });
  }

  findOne(id: number) {
    return this.checkStockRepository.findOneOrFail({
      where: { id },
      relations: { checkStockItems: true, user: true },
    });
  }

  update(id: number, updateCheckstockDto: UpdateCheckstockDto) {
    return `This action updates a #${id} checkstock`;
  }

  async remove(id: number) {
    const deleteCheckStock = await this.checkStockRepository.findOneOrFail({
      where: { id },
    });
    await this.checkStockRepository.remove(deleteCheckStock);

    return deleteCheckStock;
  }
}
