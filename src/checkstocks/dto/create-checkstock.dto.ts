export class CreateCheckstockDto {
  checkStockItems: {
    stockId: number;
    balance: number;
    amount: number;
  }[];
  userId: number;
  date: Date;
}
