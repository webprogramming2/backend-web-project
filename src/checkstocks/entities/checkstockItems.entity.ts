import { Branch } from 'src/branchs/entities/branch.entity';
import { Checkstock } from 'src/checkstocks/entities/checkstock.entity';
import { Stock } from 'src/stocks/entities/stock.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
@Entity()
export class CheckStockItem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;
  @Column()
  minimum: number;

  @Column()
  balance: number;

  @Column()
  unit: string;

  @Column()
  price: number;

  @Column()
  amount: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => Branch, (branch) => branch.stocks)
  @JoinColumn({ name: 'branchId' })
  branch: Branch;

  @ManyToOne(() => Checkstock, (checkstock) => checkstock.checkStockItems, {
    onDelete: 'CASCADE',
  })
  @JoinTable()
  checkstock: Checkstock;

  @ManyToOne(() => Stock, (stock) => stock.checkstockItems)
  stock: Stock;
}
