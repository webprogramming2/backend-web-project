export class CreateMemberDto {
  name: string;
  image: string;
  email: string;
  password: string;
  tel: string;
  point: string;
}
