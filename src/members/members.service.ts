import { Injectable } from '@nestjs/common';
import { CreateMemberDto } from './dto/create-member.dto';
import { UpdateMemberDto } from './dto/update-member.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Member } from './entities/member.entity';
import { Repository } from 'typeorm';
// import * as bcrypt from 'bcrypt';

@Injectable()
export class MembersService {
  constructor(
    @InjectRepository(Member) private membersRepository: Repository<Member>,
  ) {}
  async create(createMemberDto: CreateMemberDto) {
    const member = new Member();
    member.name = createMemberDto.name;
    member.tel = createMemberDto.tel;
    member.point = parseInt(createMemberDto.point);
    member.email = createMemberDto.email;

    // const saltOrRounds = 10;
    // member.password = await bcrypt.hash(createMemberDto.password, saltOrRounds);

    member.password = createMemberDto.password;

    if (createMemberDto.image && createMemberDto.image !== '') {
      member.image = createMemberDto.image;
    }

    return this.membersRepository.save(member);
  }

  findAll() {
    return this.membersRepository.find();
  }

  findOne(id: number) {
    return this.membersRepository.findOne({
      where: { id },
    });
  }

  findOneByTel(tel: string) {
    return this.membersRepository.findOneOrFail({
      where: { tel: tel },
    });
  }

  findOneByEmail(email: string) {
    return this.membersRepository.findOneOrFail({
      where: { email: email },
    });
  }

  async update(id: number, updateMemberDto: UpdateMemberDto) {
    const member = await this.membersRepository.findOneOrFail({
      where: { id },
    });
    member.name = updateMemberDto.name;
    member.tel = updateMemberDto.tel;
    member.point = parseInt(updateMemberDto.point);
    member.email = updateMemberDto.email;
    member.password = updateMemberDto.password;
    if (updateMemberDto.image && updateMemberDto.image !== '') {
      member.image = updateMemberDto.image;
    }
    this.membersRepository.save(member);
    return member;
  }

  async remove(id: number) {
    const deleteMember = await this.membersRepository.findOneOrFail({
      where: { id },
    });
    await this.membersRepository.remove(deleteMember);

    return deleteMember;
  }
}
