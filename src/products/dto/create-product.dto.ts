export class CreateProductDto {
  name: string;

  image: string;

  price: string;

  type: string;

  gsize: string;

  category: string;
}
