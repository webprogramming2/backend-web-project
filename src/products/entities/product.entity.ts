import { Promotion } from 'src/promotions/entities/promotion.entity';
import { ReceiptItem } from 'src/receipts/entities/receiptItem.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToMany,
  OneToMany,
} from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ default: 'noimage.jpg' })
  image: string;

  @Column()
  price: number;

  @Column()
  category: string;

  @Column({ default: '-' })
  type: string;

  @Column({ default: '-' })
  gsize: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToMany(() => Promotion, (promotion) => promotion.products)
  promotions: Promotion[];

  @OneToMany(() => ReceiptItem, (receiptItem) => receiptItem.product)
  receiptItems: ReceiptItem[];
}
