import { Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Product } from './entities/product.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product) private productsRepository: Repository<Product>,
  ) {}
  create(createProductDto: CreateProductDto) {
    const product = new Product();
    product.name = createProductDto.name;
    product.gsize = createProductDto.gsize;
    product.type = createProductDto.type;
    product.price = parseFloat(createProductDto.price);
    product.category = createProductDto.category;
    if (createProductDto.image && createProductDto.image !== '') {
      product.image = createProductDto.image;
    }
    return this.productsRepository.save(product);
  }

  findAll() {
    return this.productsRepository.find();
  }

  findOne(id: number) {
    return this.productsRepository.find({ where: { id: id } });
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    const product = await this.productsRepository.findOneOrFail({
      where: { id },
    });
    product.name = updateProductDto.name;
    product.gsize = updateProductDto.gsize;
    product.type = updateProductDto.type;
    product.price = parseFloat(updateProductDto.price);
    product.category = updateProductDto.category;
    if (updateProductDto.image && updateProductDto.image !== '') {
      product.image = updateProductDto.image;
    }
    await this.productsRepository.save(product);
    const result = await this.productsRepository.findOne({
      where: { id },
    });
    return result;
  }

  async remove(id: number) {
    const deleteProduct = await this.productsRepository.findOneOrFail({
      where: { id },
    });
    await this.productsRepository.remove(deleteProduct);
    return deleteProduct;
  }

  findAllByCategory(category: string) {
    return this.productsRepository.find({
      where: { category: category },
      order: { name: 'ASC' },
    });
  }
}
