export class CreatePromotionDto {
  condition: string;
  start: Date;
  end: Date;
  name: string;
  discount: number;
  image: string;
  products: string;
}
