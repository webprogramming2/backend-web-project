import { Injectable } from '@nestjs/common';
import { CreatePromotionDto } from './dto/create-promotion.dto';
import { UpdatePromotionDto } from './dto/update-promotion.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Promotion } from './entities/promotion.entity';
import { Repository } from 'typeorm';
import { Product } from 'src/products/entities/product.entity';

@Injectable()
export class PromotionsService {
  @InjectRepository(Promotion)
  private promotionRepository: Repository<Promotion>;
  @InjectRepository(Product) private productsRepository: Repository<Product>;
  async create(createPromotionDto: CreatePromotionDto) {
    const promotion = new Promotion();
    promotion.name = createPromotionDto.name;
    promotion.condition = createPromotionDto.condition;
    promotion.start = new Date(createPromotionDto.start);
    promotion.end = new Date(createPromotionDto.end);
    promotion.discount = createPromotionDto.discount;
    promotion.products = JSON.parse(createPromotionDto.products);
    const date = new Date();
    if (promotion.start < date && promotion.end > date) {
      promotion.status = true;
    } else {
      promotion.status = false;
    }
    if (createPromotionDto.image && createPromotionDto.image !== '') {
      promotion.image = createPromotionDto.image;
    }
    return this.promotionRepository.save(promotion);
  }

  findAll() {
    return this.promotionRepository.find({ relations: { products: true } });
  }

  // async findDetail(name: string) {
  //   return await this.promotionDetailRepository.find({
  //     where: { promoName: name },
  //   });
  // }
  findOne(id: number) {
    return this.promotionRepository.findOneOrFail({
      where: { id },
      relations: { products: true },
    });
  }

  async update(id: number, updatePromotionDto: UpdatePromotionDto) {
    const promotion = new Promotion();
    promotion.name = updatePromotionDto.name;
    promotion.condition = updatePromotionDto.condition;
    promotion.start = new Date(updatePromotionDto.start);
    promotion.end = new Date(updatePromotionDto.end);
    promotion.discount = updatePromotionDto.discount;
    promotion.products = JSON.parse(updatePromotionDto.products);
    updatePromotionDto.products = JSON.parse(updatePromotionDto.products);
    const date = new Date();
    if (promotion.start < date && promotion.end > date) {
      promotion.status = true;
    } else {
      promotion.status = false;
    }
    if (updatePromotionDto.image && updatePromotionDto.image !== '') {
      promotion.image = updatePromotionDto.image;
    }
    const updatePromotion = await this.promotionRepository.findOneOrFail({
      where: { id },
      relations: { products: true },
    });
    updatePromotion.name = promotion.name;
    updatePromotion.condition = promotion.condition;
    updatePromotion.discount = promotion.discount;
    updatePromotion.start = promotion.start;
    updatePromotion.end = promotion.end;
    updatePromotion.status = promotion.status;
    updatePromotion.image = promotion.image;
    // for (const p of promotion.products) {
    //   const searchProduct = promotion.products.find(
    //     (product) => product.id === p.id,
    //   );
    //   console.log(searchProduct);
    //   if (!searchProduct) {
    //     console.log('hit');
    //     const newRole = await this.productsRepository.findOneBy({ id: p.id });
    //     updatePromotion.products.push(newRole);
    //   }
    // }
    console.log(updatePromotion.products.length);
    console.log(updatePromotionDto.products.length);
    console.log('-----------------------------5--');
    console.log(promotion.products);
    if (updatePromotionDto.products.length > 1) {
      const j = promotion.products.length - updatePromotion.products.length;
      if (j >= 1) {
        console.log('hit');
        for (
          let index = promotion.products.length;
          index > updatePromotion.products.length;
          index--
        ) {
          console.log('id product : ' + promotion.products[index - 1].id);
          const newProduct = await this.productsRepository.findOneBy({
            id: promotion.products[index - 1].id,
          });
          updatePromotion.products.push(newProduct);
        }
      }
      //----------
      for (let i = 0; i < updatePromotion.products.length; i++) {
        const index = promotion.products.findIndex(
          (product) => product.id === updatePromotion.products[i].id,
        );
        if (index < 0) {
          updatePromotion.products.splice(i, 1);
        }
      }
    } else {
      console.log('hitto');
      updatePromotion.products[0] = promotion.products[0];
    }
    console.log('+++++++++++++++++++++++++++++++++++++++++');
    console.log(updatePromotion);
    await this.promotionRepository.save(updatePromotion);
    const result = await this.promotionRepository.findOne({
      where: { id },
      relations: { products: true },
    });
    return result;
  }

  async remove(id: number) {
    const deleteProduct = await this.promotionRepository.findOneOrFail({
      where: { id },
    });
    await this.promotionRepository.remove(deleteProduct);

    return deleteProduct;
  }
}
