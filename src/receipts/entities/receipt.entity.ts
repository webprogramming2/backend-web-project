import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ReceiptItem } from './receiptItem.entity';
import { Promotion } from 'src/promotions/entities/promotion.entity';
import { Branch } from 'src/branchs/entities/branch.entity';
import { User } from 'src/users/entities/user.entity';
import { Member } from 'src/members/entities/member.entity';

@Entity()
export class Receipt {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  date: Date;

  @Column('time')
  time: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @Column()
  channel: string;

  @Column()
  qty: number;

  @Column()
  total_before: number;

  @Column({ default: 0 })
  member_discount: number;

  @Column()
  promotion_discount: number;

  @Column()
  total_discount: number;

  @Column()
  total: number;

  @Column()
  paymentType: string;

  @Column()
  change: number;

  @Column()
  receivedAmount: number;

  @Column()
  branchId?: number;

  @Column()
  userId?: number;

  @Column({ default: '555' })
  userName?: string;

  @Column()
  memberId?: number;

  @OneToMany(() => ReceiptItem, (receiptItem) => receiptItem.receipt, {
    cascade: true,
  })
  receiptItems: ReceiptItem[];

  @ManyToMany(() => Promotion, (promotion) => promotion.receipts, {
    cascade: true,
  })
  @JoinTable()
  promotions?: Promotion[];

  @ManyToOne(() => Branch, (branch) => branch.receipts)
  branch: Branch;

  @ManyToOne(() => User, (user) => user.receipts)
  user: User;
  @ManyToOne(() => Member, (member) => member.receipts)
  member: Member;
}
