import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Receipt } from './receipt.entity';
import { Product } from 'src/products/entities/product.entity';

@Entity()
export class ReceiptItem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  unit: number;

  @Column()
  sweet_level: string;

  @Column()
  name: string;

  @Column()
  total: number;

  @Column()
  price: number;

  @Column()
  type: string;

  @Column()
  gsize: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => Receipt, (receipt) => receipt.receiptItems, {
    onDelete: 'CASCADE',
  })
  receipt: Receipt;

  @ManyToOne(() => Product, (product) => product.receiptItems)
  product: Product;
}
