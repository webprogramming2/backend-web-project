import { Injectable } from '@nestjs/common';
import { CreateReceiptDto } from './dto/create-receipt.dto';
import { UpdateReceiptDto } from './dto/update-receipt.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Receipt } from './entities/receipt.entity';
import { Repository } from 'typeorm';
import { ReceiptItem } from './entities/receiptItem.entity';
import { Product } from 'src/products/entities/product.entity';

@Injectable()
export class ReceiptsService {
  constructor(
    @InjectRepository(Receipt) private receiptsRepository: Repository<Receipt>,
    @InjectRepository(ReceiptItem)
    private receiptItemsRepository: Repository<ReceiptItem>,
    @InjectRepository(Product) private productsRepository: Repository<Product>,
  ) {}
  async create(createReceiptDto: CreateReceiptDto) {
    const receipt = new Receipt();
    receipt.receiptItems = [];
    receipt.qty = 0;
    receipt.total = 0;
    receipt.total_before = 0;
    receipt.total_discount = 0;
    receipt.promotion_discount = 0;
    receipt.member_discount = 0;
    console.log('just');
    receipt.member = createReceiptDto.member;
    if (createReceiptDto.promotions) {
      receipt.promotions = createReceiptDto.promotions;
    } else {
      receipt.promotions = null;
    }
    receipt.branch = createReceiptDto.branch;
    if (createReceiptDto.user) {
      receipt.user = createReceiptDto.user;
    } else {
      receipt.user = null;
    }
    console.log('promotion');
    receipt.userName = receipt.user.fullName;
    receipt.member_discount = createReceiptDto.member_discount;
    receipt.promotion_discount = createReceiptDto.promotion_discount;
    receipt.channel = createReceiptDto.channel;
    receipt.change = createReceiptDto.change;
    receipt.receivedAmount = createReceiptDto.receivedAmount;
    receipt.date = createReceiptDto.date;
    receipt.time = createReceiptDto.time;
    receipt.paymentType = createReceiptDto.paymentType;
    console.log('other');
    for (const io of createReceiptDto.receiptItems) {
      const receiptItem = new ReceiptItem();
      receiptItem.product = await this.productsRepository.findOneBy({
        id: io.productId,
      });
      receiptItem.name = receiptItem.product.name;
      receiptItem.price = receiptItem.product.price;
      receiptItem.unit = io.unit;
      receiptItem.sweet_level = io.sweet_level;
      receiptItem.type = io.type;
      receiptItem.gsize = io.gsize;
      receiptItem.total = receiptItem.price * receiptItem.unit;
      await this.receiptItemsRepository.save(receiptItem);
      receipt.receiptItems.push(receiptItem);
      receipt.qty += receiptItem.unit;
      receipt.total_before += receiptItem.total;
    }
    console.log('receiptItem');
    receipt.promotion_discount = createReceiptDto.promotion_discount;
    receipt.total_discount = createReceiptDto.total_discount;
    receipt.total = receipt.total_before - receipt.total_discount;
    console.log(receipt);
    return this.receiptsRepository.save(receipt);
  }

  findAll() {
    return this.receiptsRepository.find({
      relations: { receiptItems: true, promotions: true },
    });
  }

  findOne(id: number) {
    return this.receiptsRepository.find({
      where: { id: id },
      relations: { receiptItems: true, promotions: true },
    });
  }

  findByBranch(id: number) {
    console.log('service');
    return this.receiptsRepository.find({
      where: { branchId: id },
      relations: { receiptItems: true, promotions: true },
    });
  }

  update(id: number, updateReceiptDto: UpdateReceiptDto) {
    return `This action updates a #${id} receipt`;
  }

  remove(id: number) {
    return `This action removes a #${id} receipt`;
  }
}
