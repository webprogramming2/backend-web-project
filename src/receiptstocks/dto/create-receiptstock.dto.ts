export class CreateReceiptstockDto {
  receiptStockItems: {
    stockId: number;
    unitPerItem: number;
    totalprice: number;
  }[];
  userId: number;
  date: Date;
  idreceipt: string;
  nameplace: string;
}
