import { User } from 'src/users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

import { ReceiptStockItem } from './receiptstockItem.entity';

@Entity()
export class ReceiptStock {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, (user) => user.receiptStock)
  user: User;

  @Column()
  Date: Date;

  @Column()
  total: number;

  @Column()
  idreceipt: string;

  @Column()
  nameplace: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(
    () => ReceiptStockItem,
    (receiptStockItem) => receiptStockItem.receiptStock,
  )
  receiptStockItems: ReceiptStockItem[];
}
