import { Branch } from 'src/branchs/entities/branch.entity';
import { Checkstock } from 'src/checkstocks/entities/checkstock.entity';
import { Stock } from 'src/stocks/entities/stock.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ReceiptStock } from './receiptstock.entity';
@Entity()
export class ReceiptStockItem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;
  @Column()
  minimum: number;

  @Column()
  balance: number;

  @Column()
  unit: string;

  @Column()
  price: number;

  @Column()
  totalprice: number;

  @Column()
  unitPerItem: number;

  @Column()
  beforeprice: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(
    () => ReceiptStock,
    (receiptStock) => receiptStock.receiptStockItems,
    {
      onDelete: 'CASCADE',
    },
  )
  @JoinTable()
  receiptStock: ReceiptStock;

  @ManyToOne(() => Stock, (stock) => stock.checkstockItems)
  stock: Stock;
}
