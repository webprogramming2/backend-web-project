import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ReceiptstocksService } from './receiptstocks.service';
import { CreateReceiptstockDto } from './dto/create-receiptstock.dto';
import { UpdateReceiptstockDto } from './dto/update-receiptstock.dto';

@Controller('receiptstocks')
export class ReceiptstocksController {
  constructor(private readonly receiptstocksService: ReceiptstocksService) {}

  @Post()
  create(@Body() createReceiptstockDto: CreateReceiptstockDto) {
    return this.receiptstocksService.create(createReceiptstockDto);
  }

  @Get()
  findAll() {
    return this.receiptstocksService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.receiptstocksService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateReceiptstockDto: UpdateReceiptstockDto,
  ) {
    return this.receiptstocksService.update(+id, updateReceiptstockDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.receiptstocksService.remove(+id);
  }
}
