import { Module } from '@nestjs/common';
import { ReceiptstocksService } from './receiptstocks.service';
import { ReceiptstocksController } from './receiptstocks.controller';
import { ReceiptStock } from './entities/receiptstock.entity';
import { ReceiptStockItem } from './entities/receiptstockItem.entity';
import { Stock } from 'src/stocks/entities/stock.entity';
import { User } from 'src/users/entities/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forFeature([ReceiptStock, Stock, User, ReceiptStockItem]),
  ],
  controllers: [ReceiptstocksController],
  providers: [ReceiptstocksService],
})
export class ReceiptstocksModule {}
