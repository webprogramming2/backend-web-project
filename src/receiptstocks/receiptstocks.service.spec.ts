import { Test, TestingModule } from '@nestjs/testing';
import { ReceiptstocksService } from './receiptstocks.service';

describe('ReceiptstocksService', () => {
  let service: ReceiptstocksService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ReceiptstocksService],
    }).compile();

    service = module.get<ReceiptstocksService>(ReceiptstocksService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
