import { Injectable } from '@nestjs/common';
import { CreateReceiptstockDto } from './dto/create-receiptstock.dto';
import { UpdateReceiptstockDto } from './dto/update-receiptstock.dto';
import { ReceiptStock } from './entities/receiptstock.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ReceiptStockItem } from './entities/receiptstockItem.entity';
import { User } from 'src/users/entities/user.entity';
import { Stock } from 'src/stocks/entities/stock.entity';

@Injectable()
export class ReceiptstocksService {
  @InjectRepository(ReceiptStock)
  private receiptStocksRepository: Repository<ReceiptStock>;
  @InjectRepository(ReceiptStockItem)
  private receiptStockItemsRepository: Repository<ReceiptStockItem>;
  @InjectRepository(User) private usersRepository: Repository<User>;
  @InjectRepository(Stock) private stocksRepository: Repository<Stock>;

  async create(createReceiptstockDto: CreateReceiptstockDto) {
    console.log('Create RCI Function');

    const receiptStock = new ReceiptStock();
    const stock = new Stock();
    const user = await this.usersRepository.findOneBy({
      id: createReceiptstockDto.userId,
    });
    receiptStock.user = user;
    receiptStock.total = 0;
    receiptStock.idreceipt = createReceiptstockDto.idreceipt;
    receiptStock.nameplace = createReceiptstockDto.nameplace;
    receiptStock.Date = new Date(createReceiptstockDto.date);
    console.log(receiptStock.Date);
    receiptStock.receiptStockItems = [];
    console.log(receiptStock.user);

    for (const ri of createReceiptstockDto.receiptStockItems) {
      const receiptStockitem = new ReceiptStockItem();
      receiptStockitem.stock = await this.stocksRepository.findOneBy({
        id: ri.stockId,
      });
      receiptStockitem.name = receiptStockitem.stock.name;
      receiptStockitem.minimum = receiptStockitem.stock.minimum;
      receiptStockitem.balance = receiptStockitem.stock.balance;
      receiptStockitem.price = receiptStockitem.stock.price;
      receiptStockitem.unit = receiptStockitem.stock.unit;
      receiptStockitem.totalprice = ri.totalprice;
      receiptStockitem.unitPerItem = ri.unitPerItem;
      console.log(ri);
      console.log('UPI ' + ri.unitPerItem);
      await this.receiptStockItemsRepository.save(receiptStockitem);
      receiptStock.receiptStockItems.push(receiptStockitem);
      receiptStock.total += receiptStockitem.totalprice;
      stock.name = receiptStockitem.name;
      stock.minimum = receiptStockitem.minimum;
      stock.price = receiptStockitem.price;
      stock.unit = receiptStockitem.unit;
      stock.balance = receiptStockitem.balance;
      stock.balance = stock.balance + ri.unitPerItem;
      stock.branch = receiptStockitem.stock.branch;
      console.log('Stockid ' + ri.stockId);
      console.log(stock);
      await this.stocksRepository.update(ri.stockId, stock);
      console.log('Finish Create');
    }
    return this.receiptStocksRepository.save(receiptStock);
  }

  findAll() {
    return this.receiptStocksRepository.find({
      relations: {
        receiptStockItems: true,
      },
      order: {
        id: 'DESC',
      },
    });
  }

  findOne(id: number) {
    return this.receiptStocksRepository.findOneOrFail({
      where: { id },
      relations: { receiptStockItems: true, user: true },
    });
  }

  update(id: number, updateReceiptstockDto: UpdateReceiptstockDto) {
    return `This action updates a #${id} receiptstock`;
  }

  async remove(id: number) {
    const deleteReceipt = await this.receiptStocksRepository.findOneOrFail({
      where: { id },
    });
    await this.receiptStocksRepository.remove(deleteReceipt);

    return deleteReceipt;
  }
}
