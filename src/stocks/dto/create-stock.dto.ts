export class CreateStockDto {
  name: string;

  minimum: number;

  balance: number;

  unit: string;

  price: number;
  branchId: number;
}
