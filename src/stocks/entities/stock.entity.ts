import { Branch } from 'src/branchs/entities/branch.entity';
import { Checkstock } from 'src/checkstocks/entities/checkstock.entity';
import { CheckStockItem } from 'src/checkstocks/entities/checkstockItems.entity';
import { ReceiptStockItem } from 'src/receiptstocks/entities/receiptstockItem.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
@Entity()
export class Stock {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;
  @Column()
  minimum: number;

  @Column()
  balance: number;

  @Column()
  unit: string;

  @Column()
  price: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => Branch, (branch) => branch.stocks)
  @JoinColumn()
  branch: Branch;

  @OneToMany(() => CheckStockItem, (checkStockItem) => checkStockItem.stock)
  @JoinTable()
  checkstockItems: CheckStockItem[];

  @OneToMany(
    () => ReceiptStockItem,
    (receiptStockItem) => receiptStockItem.stock,
  )
  @JoinTable()
  receiptStockItem: ReceiptStockItem[];
}
