import { Injectable } from '@nestjs/common';
import { CreateStockDto } from './dto/create-stock.dto';
import { UpdateStockDto } from './dto/update-stock.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Stock } from './entities/stock.entity';
import { Repository } from 'typeorm';
import { Branch } from 'src/branchs/entities/branch.entity';

@Injectable()
export class StocksService {
  constructor(
    @InjectRepository(Stock)
    private stocksRepository: Repository<Stock>,
    @InjectRepository(Branch)
    private branchsRepository: Repository<Branch>,
  ) {}
  async create(createStockDto: CreateStockDto) {
    const stock = new Stock();
    const branch = await this.branchsRepository.findOneBy({
      id: createStockDto.branchId,
    });
    stock.name = createStockDto.name;
    stock.minimum = createStockDto.minimum;
    stock.balance = createStockDto.balance;
    stock.price = createStockDto.price;
    stock.unit = createStockDto.unit;
    stock.branch = branch;

    return this.stocksRepository.save(stock);
  }

  findAll() {
    return this.stocksRepository.find({
      relations: { branch: true },
    });
  }

  findOne(id: number) {
    console.log('fineone');
    return this.stocksRepository.find({
      where: { id: id },
      relations: { branch: true },
    });
  }
  async findAllByBranch(branchId: number) {
    const branch = JSON.stringify(
      await this.branchsRepository.findOneBy({
        id: branchId,
        stocks: true,
      }),
    );
    console.log(branch);

    const stocks = await this.stocksRepository.find({
      where: { branch: JSON.parse(branch) },
    });

    console.log(stocks);

    return stocks;
  }

  async update(id: number, updateStockDto: UpdateStockDto) {
    await this.stocksRepository.update(id, updateStockDto);
    const stock = await this.stocksRepository.findOneBy({ id });
    return stock;
  }

  async remove(id: number) {
    const deleteStock = await this.stocksRepository.findOneOrFail({
      where: { id },
    });
    this.stocksRepository.remove(deleteStock);
    return deleteStock;
  }
}
