import { PartialType } from '@nestjs/mapped-types';
import { CreateTypebillcostDto } from './create-typebillcost.dto';

export class UpdateTypebillcostDto extends PartialType(CreateTypebillcostDto) {}
