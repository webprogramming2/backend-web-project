import { Test, TestingModule } from '@nestjs/testing';
import { TypebillcostsController } from './typebillcosts.controller';
import { TypebillcostsService } from './typebillcosts.service';

describe('TypebillcostsController', () => {
  let controller: TypebillcostsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TypebillcostsController],
      providers: [TypebillcostsService],
    }).compile();

    controller = module.get<TypebillcostsController>(TypebillcostsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
