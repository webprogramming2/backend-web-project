import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { TypebillcostsService } from './typebillcosts.service';
import { CreateTypebillcostDto } from './dto/create-typebillcost.dto';
import { UpdateTypebillcostDto } from './dto/update-typebillcost.dto';
import { AuthGuard } from 'src/auth/auth.guard';

@UseGuards(AuthGuard)
@Controller('typebillcosts')
export class TypebillcostsController {
  constructor(private readonly typebillcostsService: TypebillcostsService) {}

  @Post()
  create(@Body() createTypebillcostDto: CreateTypebillcostDto) {
    return this.typebillcostsService.create(createTypebillcostDto);
  }

  @Get()
  findAll() {
    return this.typebillcostsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.typebillcostsService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateTypebillcostDto: UpdateTypebillcostDto,
  ) {
    return this.typebillcostsService.update(+id, updateTypebillcostDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.typebillcostsService.remove(+id);
  }
}
