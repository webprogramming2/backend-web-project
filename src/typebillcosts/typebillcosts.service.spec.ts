import { Test, TestingModule } from '@nestjs/testing';
import { TypebillcostsService } from './typebillcosts.service';

describe('TypebillcostsService', () => {
  let service: TypebillcostsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TypebillcostsService],
    }).compile();

    service = module.get<TypebillcostsService>(TypebillcostsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
