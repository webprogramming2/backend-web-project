import { Injectable } from '@nestjs/common';
import { CreateTypebillcostDto } from './dto/create-typebillcost.dto';
import { UpdateTypebillcostDto } from './dto/update-typebillcost.dto';
import { Typebillcost } from './entities/typebillcost.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class TypebillcostsService {
  constructor(
    @InjectRepository(Typebillcost)
    private typebillcostsRepository: Repository<Typebillcost>,
  ) {}
  create(createTypebillcostDto: CreateTypebillcostDto) {
    return 'This action adds a new typebillcost';
  }

  findAll() {
    return this.typebillcostsRepository.find();
  }

  findOne(id: number) {
    return `This action returns a #${id} typebillcost`;
  }

  update(id: number, updateTypebillcostDto: UpdateTypebillcostDto) {
    return `This action updates a #${id} typebillcost`;
  }

  remove(id: number) {
    return `This action removes a #${id} typebillcost`;
  }
}
