export class CreateUserDto {
  image: string;
  fullName: string;
  email: string;
  password: string;
  gender: string;
  role: string;
  branch: string;
  worktime: string;
}
