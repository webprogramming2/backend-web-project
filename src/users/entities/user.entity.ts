import { Billcost } from 'src/billcosts/entities/billcost.entity';
import { Branch } from 'src/branchs/entities/branch.entity';
import { Worktime } from 'src/worktimes/entities/worktime.entity';
import { Receipt } from 'src/receipts/entities/receipt.entity';
import { Checkstock } from 'src/checkstocks/entities/checkstock.entity';
import { ReceiptStock } from 'src/receiptstocks/entities/receiptstock.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  JoinColumn,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  JoinTable,
} from 'typeorm';
import { Checkinout } from 'src/checkinouts/entities/checkinout.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: 'noimage.jpg' })
  image: string;

  @Column()
  email: string;

  @Column({
    name: 'full-name',
    default: '',
  })
  fullName: string;

  @Column()
  password: string;

  @Column()
  gender: string;

  @Column()
  role: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => Branch, (branch) => branch.manager)
  @JoinColumn({ name: 'branch_id' })
  branch: Branch;

  @OneToMany(() => Billcost, (billcost) => billcost.User)
  billcosts: Billcost[];

  @ManyToOne(() => Worktime, (worktime) => worktime.user)
  @JoinColumn({ name: 'worktime_id' })
  worktime: Worktime;
  @OneToMany(() => Receipt, (receipt) => receipt.user)
  receipts: Receipt[];
  @OneToMany(() => Checkstock, (checkstock) => checkstock.user, {
    cascade: true,
  })
  @JoinTable()
  checkstock: Checkstock[];

  @OneToMany(() => ReceiptStock, (receiptStock) => receiptStock.user, {
    cascade: true,
  })
  @JoinTable()
  receiptStock: ReceiptStock[];

  @OneToMany(() => Checkinout, (checkinout) => checkinout.user)
  checkinouts: Checkinout[];
}
