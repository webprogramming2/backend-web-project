/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
// import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private usersRepository: Repository<User>,
  ) {}
  // lastId: number = 1;
  // users: User[] = [
  //   {
  //     id: 1,
  //     email: 'yamyen@gmail.com',
  //     password: '12345678',
  //     fullname: 'Piyaphon Yamyen',
  //     branch: 'สหเวช',
  //     gender: 'male',
  //     role: 'manager',
  //   },
  // ];
  async create(createUserDto: CreateUserDto) {
    const user = new User();
    user.fullName = createUserDto.fullName;

    // const saltOrRounds = 10;
    // user.password = await bcrypt.hash(createUserDto.password, saltOrRounds);

    user.password = createUserDto.password;
    user.email = createUserDto.email;
    user.gender = createUserDto.gender;
    user.role = createUserDto.role;
    user.branch = JSON.parse(createUserDto.branch);
    user.worktime = JSON.parse(createUserDto.worktime);
    if (createUserDto.image && createUserDto.image !== '') {
      user.image = createUserDto.image;
    }

    return this.usersRepository.save(user);
  }

  findAll() {
    return this.usersRepository.find({
      relations: { branch: true, worktime: true },
    });
  }

  findOne(id: number) {
    return this.usersRepository.findOne({
      where: { id },
      relations: { worktime: true, branch: true },
    });
  }

  async findOneByEmail(email: string) {
    const user = await this.usersRepository.findOneOrFail({
      where: { email },
      relations: { branch: true },
    });
    console.log(user);

    return this.usersRepository.findOneOrFail({
      where: { email },
      relations: { branch: true },
    });
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const user = await this.usersRepository.findOneOrFail({
      where: { id },
    });
    user.fullName = updateUserDto.fullName;
    user.password = updateUserDto.password;
    user.email = updateUserDto.email;
    user.gender = updateUserDto.gender;
    user.role = updateUserDto.role;
    user.branch = JSON.parse(updateUserDto.branch);
    user.worktime = JSON.parse(updateUserDto.worktime);
    console.log(updateUserDto.fullName);
    console.log(user.fullName);
    console.log(user.branch);
    if (updateUserDto.image && updateUserDto.image !== '') {
      user.image = updateUserDto.image;
    }
    this.usersRepository.save(user);
    return user;
  }

  async remove(id: number) {
    const deleteUser = await this.usersRepository.findOneOrFail({
      where: { id },
    });
    await this.usersRepository.remove(deleteUser);

    return deleteUser;
  }
}
