import { Test, TestingModule } from '@nestjs/testing';
import { WorktimesController } from './worktimes.controller';
import { WorktimesService } from './worktimes.service';

describe('WorktimesController', () => {
  let controller: WorktimesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [WorktimesController],
      providers: [WorktimesService],
    }).compile();

    controller = module.get<WorktimesController>(WorktimesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
