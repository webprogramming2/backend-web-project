import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { WorktimesService } from './worktimes.service';
import { CreateWorktimeDto } from './dto/create-worktime.dto';
import { UpdateWorktimeDto } from './dto/update-worktime.dto';

@Controller('worktimes')
export class WorktimesController {
  constructor(private readonly worktimesService: WorktimesService) {}

  @Post()
  create(@Body() createWorktimeDto: CreateWorktimeDto) {
    return this.worktimesService.create(createWorktimeDto);
  }

  @Get()
  findAll() {
    return this.worktimesService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.worktimesService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateWorktimeDto: UpdateWorktimeDto,
  ) {
    return this.worktimesService.update(+id, updateWorktimeDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.worktimesService.remove(+id);
  }
}
