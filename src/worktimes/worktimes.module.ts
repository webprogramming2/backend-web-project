import { Module } from '@nestjs/common';
import { WorktimesService } from './worktimes.service';
import { WorktimesController } from './worktimes.controller';
import { Worktime } from './entities/worktime.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Worktime])],
  controllers: [WorktimesController],
  providers: [WorktimesService],
  exports: [WorktimesService],
})
export class WorktimesModule {}
