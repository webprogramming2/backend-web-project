import { Test, TestingModule } from '@nestjs/testing';
import { WorktimesService } from './worktimes.service';

describe('WorktimesService', () => {
  let service: WorktimesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [WorktimesService],
    }).compile();

    service = module.get<WorktimesService>(WorktimesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
