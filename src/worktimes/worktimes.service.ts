import { Injectable } from '@nestjs/common';
import { CreateWorktimeDto } from './dto/create-worktime.dto';
import { UpdateWorktimeDto } from './dto/update-worktime.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Worktime } from './entities/worktime.entity';
import { Repository } from 'typeorm';

@Injectable()
export class WorktimesService {
  constructor(
    @InjectRepository(Worktime)
    private worktimesRepository: Repository<Worktime>,
  ) {}
  create(createWorktimeDto: CreateWorktimeDto) {
    return 'This action adds a new worktime';
  }

  findAll() {
    console.log('findall');
    return this.worktimesRepository.find({ relations: { user: true } });
  }

  findOne(id: number) {
    return `This action returns a #${id} worktime`;
  }

  update(id: number, updateWorktimeDto: UpdateWorktimeDto) {
    return `This action updates a #${id} worktime`;
  }

  remove(id: number) {
    return `This action removes a #${id} worktime`;
  }
}
